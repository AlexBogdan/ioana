/*
 * Stefan Ioana
 * 336 CB
 */

/*
 * Tema 2 ASC
 * 2018 Spring
 * Catalin Olaru / Vlad Spoiala
 */
#include "utils.h"

/*
 * Add your optimized implementation here
 */
double* my_solver(int N, double *A) {
	printf("OPT SOLVER\n");

	int i, j, l;
	double *C = (double *) calloc (N * N * 2, sizeof(double));

	for(i = 0; i < N; i++){
		double *orig_pa = &A[i*2*N];
		for(j = i; j < N; j++){
			double *pa = orig_pa;
			double *pb = &A[2*j*N];
			register double real = 0;
			register double imag = 0;

			for(l = 0; l < N; l++){
				real += *pa * *pb - *(pa+1) * * (pb+1);
				imag += *pa * *(pb+1) + *(pa+1) * *pb;
				
				pa += 2;
				pb += 2;
			}

			C[2 * (i * N + j)] = real;
			C[2 * (i * N + j) + 1] = imag;
		}
	}

	return C;	
}
