/*
 * Stefan Ioana
 * 336 CB
 */

/*
 * Tema 2 ASC
 * 2018 Spring
 * Catalin Olaru / Vlad Spoiala
 */

#include <cblas.h>
#include "utils.h"

/* 
 * Add your BLAS implementation here
 */
double* my_solver(int N, double *A) {
	printf("BLAS SOLVER\n");

	double *C = (double *) calloc (N * N * 2, sizeof(double));
	double ALPHA[] = {1, 0};
	double BETA[] = {0, 0};

	cblas_zsyrk(101, 121, 111, N, N, ALPHA, A, N, BETA, C, N);

	return C;
}
