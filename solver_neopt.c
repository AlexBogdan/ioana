/*
 * Stefan Ioana
 * 336 CB
 */

/*
 * Tema 2 ASC
 * 2018 Spring
 * Catalin Olaru / Vlad Spoiala
 */
#include "utils.h"

/*
 * Add your unoptimized implementation here
 */
double* my_solver(int N, double *A) {
	printf("NEOPT SOLVER\n");

	int i, j, l;
	double *C = (double *) calloc (N * N * 2, sizeof(double));

	for (i = 0; i < N; ++i) {
		for (j = i; j < N; ++j) {
			for (l = 0; l < N; ++l) {
				double x = A[2 * (i * N + l)];
				double y = A[2 * (i * N + l) + 1];
				double u = A[2 * (j * N + l)];
				double v = A[2 * (j * N + l) + 1];

				C[2 * (i * N + j)] += x * u - y * v;
				C[2 * (i * N + j) + 1] += x * v + y * u;
			}
		}
	}

	return C;
}
